require 'test_helper'

class FeedEntryTest < ActiveSupport::TestCase

	def setup
		@entry = FeedEntry.new
		@entry.name = "Test"
		@entry.summary = "Lorem Ipsum"
		@entry.published_at = Date.current
		@entry.guid = "qwerty"
		@entry.url = "http://www.example.org"
	end

	test "should not save a feed entry without parent url" do
		assert !@entry.save
	end

	test "invalid url should not be added" do
		assert !FeedEntry.addSubscription("http://www.google.com")
	end

	test "instantly updating feed should return nil" do
		feed = Feedzirra::Feed.fetch_and_parse("https://xkcd.com/rss.xml")
		assert_nil Feedzirra::Feed.update(feed)
	end
	
	test "identical guids shouldn't get added" do
		FeedEntry.add_entries(Feedzirra::Feed.fetch_and_parse("https://xkcd.com/rss.xml").entries, "test")
		total = FeedEntry.count
		FeedEntry.add_entries(Feedzirra::Feed.fetch_and_parse("https://xkcd.com/rss.xml").entries, "test")
		newtotal = FeedEntry.count
		assert_equal newtotal, total	
	end 

	test "removal of existing subscription should clean table" do
		old = FeedEntry.count
		FeedEntry.removeSubscription("MyString")
		new = FeedEntry.count
		assert_equal old - 2, new
	end
end
