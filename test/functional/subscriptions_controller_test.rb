require 'test_helper'

class SubscriptionsControllerTest < ActionController::TestCase
	include Devise::TestHelpers
	
	def setup
		@request.env["devise.mapping"] = Devise.mappings[:user]
		@user = FactoryGirl.create(:user)
		@subscription = FactoryGirl.create(:subscription, :user_id => @user.id)
		session[:user_id] = @user.id
	end

	test "signed out should see welcome page" do
		session[:user_id] = nil
		get "index"
		assert_select 'title', "rsstful | please log in"
	end

	test "show" do
		get :show, :id => @subscription.id
		assert response.body.include?("Feed at:")
	end

	test "create" do

		assert_difference('Subscription.count') do
			post :create, location: 'http://feeds.feedburner.com/Explosm'
		end
	end

	test "delete" do

		assert_difference('Subscription.count', -1) do
			delete :destroy, :id => @subscription.id
		end
	end

end
