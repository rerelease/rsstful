class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :location
      t.datetime :last
      t.references :user

      t.timestamps
    end
    add_index :subscriptions, :user_id
  end
end
