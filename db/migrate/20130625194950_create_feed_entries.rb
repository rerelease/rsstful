class CreateFeedEntries < ActiveRecord::Migration
  def change
    create_table :feed_entries do |t|
      t.string :name
      t.string :summary
      t.string :url
      t.datetime :published_at
      t.string :guid
	  t.string :feed_url

      t.timestamps
    end
  end
end
