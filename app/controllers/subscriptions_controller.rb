class SubscriptionsController < ApplicationController

	def index
	end

	def show	
		if user_signed_in?
			begin
			@subscription = Subscription.find(params[:id])
			@subscription = nil unless @subscription.user_id == current_user.id
			rescue
			end
		end
	end

	def create
		newsub = Subscription.new(params[:subscription])
		insert = true
		current_user.subscriptions.each { |sub|
			insert = false if sub.location == newsub.location
		}

		if insert
			if FeedEntry.addSubscription(newsub.location)
				@subscription = current_user.subscriptions.create(params[:subscription])
				render 'index'
			else
				flash[:notice] = 'URL contains no entries.'
				render 'index'
			end
		else
			flash[:notice] = 'You are already subscribed to this URL.'
			render 'index'
		end
	end

	def destroy
		sub = Subscription.find(params[:id])
		if sub.user_id == current_user.id
			sub.destroy
			FeedEntry.removeSubscription(sub.location) if Subscription.where(location: sub.location) == []
		end

		redirect_to subscriptions_path
	end

end
