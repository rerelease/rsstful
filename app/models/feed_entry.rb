class FeedEntry < ActiveRecord::Base
	attr_accessible :guid, :name, :published_at, :summary, :url, :feed_url
	validates_presence_of :guid, :published_at, :url, :feed_url

	def self.update_all_feeds_continuously(delay_interval = 10.minutes)
		feed_urls = Subscription.pluck("DISTINCT location")
		feeds = Feedzirra::Feed.fetch_and_parse(feed_urls)
		feeds.each { |k, f| add_entries(f.entries, k) unless f == 0 }
		loop do

			# add/remove feeds
			new_feeds = Subscription.pluck("DISTINCT location")
			if new_feeds != feed_urls
				new_feeds.each do |url|
					if feeds[url] == nil
						feeds.merge!( url => Feedzirra::Feed.fetch_and_parse(url) )
					end
				end

				feed_urls.each do |url|
					unless new_feeds.include?(url)
						feeds.except!(url)
					end
				end
				feed_urls = new_feeds
			end

			#feeds = Feedzirra::Feed.fetch_and_parse(feed_urls)
			feeds.each do |k, f| 
				if f = []
					f = Feedzirra::Feed.fetch_and_parse(k)
					add_entries(f.entries, k)
				end

				updated = Feedzirra::Feed.update(f)
				unless updated == nil 
					add_entries(updated.entries, k)
				end
			end

		end

		# since Feedzirra update fails on feeds with no etags,
		# we'll have to do this weird manual thing
		# perhaps this can be solved one day

		# old code
		#update existing feeds
		#updated = Feedzirra::Feed.update(feeds.values)
		#
		#if updated != nil
		#	feeds = updated
		#	feeds.each do |f| 
		#		add_entries(f.new_entries, f.feed_url) if f.updated?
		#	end
	end

def self.addSubscription(location)
	feed = Feedzirra::Feed.fetch_and_parse(location)
	if feed != 0 && feed != nil
		add_entries(feed.entries, location)
		return true
	else
		return false
	end
end

def self.removeSubscription(location)
	# this is mainly a cleanup method
	# new feeds of the name won't be collected
	# so we just remove old entries
	destroy_all(:feed_url => location)
end

private

def self.add_entries(entries, feed_url)
	entries.each do |entry|
		unless exists? :guid => entry.id
			create!(
				:name         => entry.title,
				:summary      => entry.summary ? entry.summary : entry.content,
				:url          => entry.url,
				:published_at => entry.published,
				:feed_url     => feed_url,
				:guid         => entry.id
			)
		end
	end
end

end
